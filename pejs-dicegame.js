/* =============================================================
     ===== CANVAS =====
     ===== dicegame ====
 script written by Petukhovsky
 http://petukhovsky.com


 TO DO
 добавить спрайт фишки
 разметить поле массивом координат




 =============================================================
*/

"use strict"; //All my JavaScript written in Strict Mode http://ecma262-5.com/ELS5_HTML.htm#Annex_C

(function(){
    // ======== private vars ========
    var clientver = "v.0.0.1";
    var htmlid = "pejs-dicegame-1";

    var srvaddress = "";
    //var gameserver = srvaddress + "?";
    var graphicsdir = srvaddress + "img/";

    var fps = 0, fpsscore = 0; //Счётчик FPS
    var canvas, ctx; //Объекты canvas, context
    var canvasbuf; //Буфер для рисования фона
    var canvastreebuf; //Буфер для ранее нарисованных объектов
    var pixeldata;

    var contwidth =  1000; //document.documentElement.scrollWidth - 10; //Ширина контейнера
    var contheight = 1000; // document.documentElement.scrollHeight - 20; //Высота контейнера

    var contcolor = "#777780"; //Цвет заливки
    var rcontcolor = (("0x"+contcolor.slice(1)) & 0xff0000) >> 16; //R компонента в DEC от contcolor
    var x, y; //Переменные необходимые для построения дерева

    var ldu = 0, tpldu = 0; //time passed from last draw update
    var requestAnimFrame; //Функция анимации

    var appstatus = "load"; // Состояния приложения  -1 (load) - приложение грузится, 0 (startmenu) - загрузка прошла, 1(run) - работа, 2(menu) - меню, 3(over) - работа окончена

    var load = {point: 0, total: 3}; //всего загружаем 2 элемента и 1 для подключения

    var ui_font_color = "#6f6864"; //Цвет шрифта по умолчанию
    var ui_font_style = "9pt Verdana"; //([font style][font weight][font size][font face]).
    var ui_font_warning_color = "#ff0000"; //Красный

    var starttime = 0;
    var totalanimtime = 90000; //время сцены с мс

    var animtime =0;

    var imgbase, imganim, imgpart; //Изображения

    var logo = {type: "img", x: contwidth/2-200, y: 250, ox: 604, oy: 20, w: 468, h: 58};
    var logotext = {type: "text", x: 300, y: 150, text: ""};
    var dicegamefield = {type: "img", x: 0, y: 0, ox: 1, oy: 549, w: 1000, h: 1000};
    var logodog = {type: "img", x: contwidth/2+105, y: contheight-145, ox: 654, oy: 254, w: 110, h: 145};

    var fpsobject = {type: "text", x: 16, y: 16, text: "FPS: "};

    ////////////////////////////////////////////////////////////////////////////
    var init = function () {
        load.point = 0;
        load.total = 3; //всего загружаем 2 элемента + 1 статус что всё ОК

        document.getElementById(htmlid+"-div").style.display = "none";
        document.getElementById(htmlid+"-run").disabled = true;
        document.getElementById(htmlid+"-run").onclick = animation_run;

        console.log("init function");

        // ---- init script ----
        canvas = document.getElementById(htmlid + "-canvas"); //Хватаем объект
        canvas.width = contwidth; //Задаём ширину холста
        canvas.height = contheight;  //Задаём ширину холста
        ctx = canvas.getContext("2d"); //Инициализируем работу с контекстом. Контекст позволяет работать с canvas применяя методы рисования

        // Если ничего нет - возвращаем обычный таймер
        requestAnimFrame = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };

        // ---- fps count ----
        setInterval(function () {
            fpsobject.text = "FPS: " + (fps * 2);
            fpsscore += fps;
            fps = 0;

        }, 500); // update every 1/2 seconds

        // ---- screen size ----
        resize();
        window.addEventListener("resize", resize, false);

        console.log("init OK");
        
        
        load_app(); //Загружаем ресурсы и т.п.
    };


    var resize = function () { //Не используется
        // ---- screen resize ----
        //nx = findPosX(canvas);
        //ny = findPosY(canvas);
    };


//////////////////////////////ANIMATION LOGIC/////////////////////////////////////

    var animation_run = function () {
        if (appstatus != "startmenu") return; //Загрузка не прошла или мы по середине игры то не стартуем
        appstatus = "run";

        starttime = (new Date()).getTime();

        document.getElementById(htmlid+"-div").style.display = "none"; //Скрываем элементы управления

        // Создаём буфер нужного размера:
        canvasbuf = document.createElement("canvas");
        canvasbuf.width = contwidth;
        canvasbuf.height = contheight;
        canvasbuf.ctx = canvasbuf.getContext("2d");

        canvasbuf.ctx.fillStyle = contcolor; // цвет фона
        canvasbuf.ctx.strokeStyle = '#020'; //цвет линий
        canvasbuf.ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvasbuf.ctx.lineWidth = 2; // ширина линий

        //Создаём буфер нужного размера для готовых частей дерева
        canvastreebuf = document.createElement("canvas");
        canvastreebuf.width = contwidth;
        canvastreebuf.height = contheight;
        canvastreebuf.ctx = canvastreebuf.getContext("2d");

        canvastreebuf.ctx.fillStyle = contcolor; // цвет фона
        canvastreebuf.ctx.strokeStyle = '#020'; //цвет линий
        canvastreebuf.ctx.lineWidth = 2; // ширина линий
        canvastreebuf.ctx.fillRect(0, 0, canvas.width, canvas.height); 

        //Вывод 32
        drawobj(dicegamefield, canvastreebuf.ctx);

        processloop();

        ldu = (new Date()).getTime(); //last draw update
        animloop();
    };

    var load_app = function () {

        imgbase = new Image();	// Создание нового объекта изображения
        imgbase.src = graphicsdir + "base.png";	// Путь к изображениям
        imgbase.onload = drawloading;   // Событие onLoad, ждём момента пока загрузится изображение

        /*
        imganim = new Image();
        imganim.src = graphicsdir + "anim.png";
        imganim.onload = drawloading;   // Событие onLoad, ждём момента пока загрузится изображение

        imgpart = new Image();
        imgpart.src = graphicsdir + "part.png";
        imgpart.onload = drawloading;
        */


        drawloading();
        drawloading();

    };

    
    var run_startmenu = function () { //Выполняем по завершении загрузки

        draw_welcome();
        appstatus = "startmenu"; //Загрузка OK

        document.getElementById(htmlid+"-div").style.display = "inline"; //Показываем блок с кнопкой
        document.getElementById(htmlid+"-run").disabled = false; //Активируем кнопку
    };


    ////////////////////////////////////////////////////////////////////////////
    var drawloading = function () { //Функция отображения загрузки и также учета прогресса загрузки
        load.point++;

        ctx.fillStyle = contcolor; //Заливаем цветом по умолчанию
        ctx.fillRect(0, 0, canvas.width, canvas.height); //Закрашивание экрана

        ctx.fillStyle = ui_font_color;

        //Рисование прогресса загрузки
        ctx.strokeRect(Math.floor(contwidth / 4), Math.floor(contheight / 2 - 6), Math.floor(contwidth / 2), 12);
        ctx.fillRect(Math.floor(contwidth / 4 + 1), Math.floor(contheight / 2 - 4), Math.floor(contwidth / 2 / load.total * load.point), 8);

        if (load.point >= load.total) {//Загрузка успешно завершена
             run_startmenu();
        }
    };

    var draw_scene = function () {

        tpldu = (new Date()).getTime() - ldu; //time passed last draw update
        ldu = (new Date()).getTime();
        fps++;

        animtime = starttime - ldu;

        if (appstatus == "run") {
            //canvasbuf.ctx.clearRect(0, 0, canvas.width, canvas.height); //Очищаем буфер, но в этом нет нужды, так как его перезаписываем буфером с деревьями
            canvasbuf.ctx.drawImage(canvastreebuf, 0, 0); //Помещаем в качестве подложки ранее отрисованные элементы деревьев                 
            canvasbuf.ctx.lineWidth = 2;
            canvastreebuf.ctx.lineWidth = 2;

         

            canvasbuf.ctx.lineWidth = 3;
            canvastreebuf.ctx.lineWidth = 3;
            pixeldata = canvastreebuf.ctx.getImageData(0, 0, contwidth, contheight).data;

            //Копирование отрендереного кадра из буфера
            ctx.drawImage(canvasbuf, 0, 0);

            ctx.fillStyle = ui_font_color; //Устанавливаем цвет шрифта
            ctx.font = ui_font_style;      //Устанавливаем стиль шрифта
            ctx.textBaseline = "top";      //Текст выравнивается по верхнему углу

            //Вывод информации об игроке и его местоположении
            ctx.textAlign = "left";
            //drawobj(ui_player_text);

            //Вывод информации о времени ответа, версии клиента и прочем
            drawobj(fpsobject);

            //Вывод времени и количества кристаллов
            //drawtime();
            //drawcrystals();

            // Рисуем игровой мир
            /*
            for (var i = 0; i < activeobjects.length; i++) objects[activeobjects[i]].draw();
            var temparray = [];
            for (var i = 0; i < activeobjects.length; i++) if (deleteid[activeobjects[i]] != true) temparray.push(activeobjects[i]);
            activeobjects = temparray;
            deleteid = {};
            */
        }

        ctx.restore();
    };

    var draw_welcome = function () {
        ctx.fillStyle = contcolor; //Заливаем цветом по умолчанию
        ctx.fillRect(0, 0, canvas.width, canvas.height); //Закрашивание экрана

        //Вывод welcome
        drawobj(logo);

        ctx.textAlign = "left";
        drawobj(logotext);
    };

    var drawobj = function (_obj, _ctx=ctx) { //Рисоваие объекта по собственным координатам

        //Вывод объекта
        if (_obj.type == "img") {
            _ctx.drawImage(imgbase, //картинка
                _obj.ox, _obj.oy,
                _obj.w, _obj.h,
                _obj.x, _obj.y, //X,Y
                _obj.w, _obj.h
            );
        } else if (_obj.type == "text") {
            _ctx.save();
            if (undef(_obj.color)) _ctx.fillStyle = ui_font_color;
            else _ctx.fillStyle = _obj.color;

            if (undef(_obj.style)) _ctx.font = ui_font_style;
            else _ctx.font = _obj.style;

            _ctx.fillText(_obj.text, _obj.x, _obj.y);
            _ctx.restore();
        }
    };

    var drawobjc = function (_obj, _x, _y) { //Рисоваие объекта по координатам из параметров
        _obj.x = _x;
        _obj.y = _y;
        drawobj(_obj);
    };

    var drawobjx = function (_obj, _x) {
        _obj.x = _x;
        drawobj(_obj);
    };
    var drawobjy = function (_obj, _y) {
        _obj.y = _y;
        drawobj(_obj);
    };

    
    ///////////////////////////////////TOOLS///////////////////////////////////////
    var random = function (min, max) {
        //min = Math.ceil(min);
        //max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }

    /*
    var xabs = function (_x) {
        if (_x > -2 && _x < 11) return _x;
        if (_x + mapsizex > -2 && _x + mapsizex < 11) return _x + mapsizex;
        return _x - mapsizex;
    }

    var yabs = function (_y) {
        if (_y > -2 && _y < 11) return _y;
        if (_y + mapsizey > -2 && _y + mapsizey < 11) return _y + mapsizey;
        return _y - mapsizey;
    }

    var inv_button_active = function () {
        var count = 0;
        for (var i = 0; i < inventory.size; i++) {
            if (def(inventory.obj[i]) && inventory.obj[i].type == this.cond1) count += inventory.obj[i].num;
        }
        return count >= this.cond2;
    }

    var obj_button_active = function () {
        var count = 0;
        for (var i = 0; i < objinv.len; i++) {
            if (def(objinv.obj[i]) && objinv.obj[i].type == this.cond1) count += objinv.obj[i].num;
        }
        return count >= this.cond2;
    }

    var clone = function (obj) {
        if (obj == null || typeof(obj) != "object")
            return obj;

        var temp = obj.constructor();

        for (var key in obj)
            temp[key] = clone(obj[key]);
        return temp;
    }

    */

    var undef = function (obj) {
        return typeof obj === "undefined";
    };

    var def = function (obj) {
        return !undef(obj);
    };

    //////////////////////////////MAIN LOOP/////////////////////////////////////
    var processloop = function () {
        if (appstatus != "run" && appstatus != "menu") return; //Не выполнять, если приложение не запущено

        if ((new Date()).getTime() > starttime + totalanimtime ) { //Анимация завершена
            appstatus = "startmenu";
            console.log("ANIMATION IS OVER, FPS SCORE = "+fpsscore);         
            setTimeout(run_startmenu, 100);
        }

        if (appstatus == "run" || appstatus == "menu") setTimeout(processloop, 500);
    };

    var animloop = function () {
        draw_scene();
        if (appstatus == "run" || appstatus == "menu") {
            requestAnimFrame(animloop);
        }
    };

    return {
        ////////////////////////////////////////////////////////////////////////////
        // ---- onload event ----
        load: function () {
            console.log("load function");
            window.addEventListener("load", function () {
                init();
            }, false);
        }
    }
})().load();


